#@ Behrooz Keshtgar, KIT 2024

# This script is for coarse-graining and calculating CRH uncertainty as a function of horizontal scale
# MYSTIC solver

#### Loading libraries
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr

# Dictionary for loading datasets for the 4 simulations
simdict = {
         'lem_domain01' : {'name':'Shallow cumulus'          ,'res':'300', 'radiation':4, 'mphy':4}, # Only cloud radiation
         'lem_domain02' : {'name':'WCB ascent'               ,'res':'300', 'radiation':4, 'mphy':4}, # Only cloud radiation
         'lem_domain03' : {'name':'WCB cyclonic outflow'     ,'res':'300', 'radiation':4, 'mphy':4}, # Only cloud radiation
         'lem_domain04' : {'name':'WCB anticyclonic outflow' ,'res':'300', 'radiation':4, 'mphy':4}  # Only cloud radiation
          }

#####################################################
##### Step 1, loading CRH from different calculations
####################################################

# all libradtrn calculations 
def load_simulations1(num):
    ds_list = []
    sim = list(simdict.keys())[num]
    print('Working on loading data for', sim)
    path = '/work/bb1135/b381185/icon_output/data_for_crh_unc_paper/offline_radiation_calculation_output/'+sim+'/'
    for solver in ['ipa3d','mysti','mcipa']:
        # loop over sources (thermal/solar)
        for source in ['thermal','solar']:
            # loop over sim types
            for nsim in ['01']: # simulations with Fu for ice param
                if solver in ['mysti','mcipa']:
                    ds = xr.open_mfdataset(path+'ds_librad_05T*_'+source+'_'+solver+'_'+nsim+'.nc',concat_dim='time',combine='nested',
                                           chunks={'height': 50},parallel=True).isel(height=slice(0,140))
                else:
                    ds = xr.open_mfdataset(path+'ds_librad2_05T*_'+source+'_'+solver+'_'+nsim+'.nc',concat_dim='time',combine='nested',
                                           chunks={'height': 50},parallel=True).isel(height=slice(0,140))
                ds.attrs['sim_name'] = 'ds_librad_'+source+'_'+solver+'_'+nsim+''
                ds_list.append(ds)
    return ds_list                
#-------------------------------------
ds_lib1=load_simulations1(0)
ds_lib2=load_simulations1(1)
ds_lib3=load_simulations1(2)
ds_lib4=load_simulations1(3)

# libradtrn simulation using other ice parametrization
def load_simulations2(num):
    ds_list = []
    sim = list(simdict.keys())[num]
    print('Working on loading data for', sim)
    path = '/work/bb1135/b381185/icon_output/data_for_crh_unc_paper/offline_radiation_calculation_output/'+sim+'/'
    for solver in ['ipa3d']:
        # loop over sources (thermal/solar)
        for source in ['thermal','solar']:
            # loop over sim types
            for nsim in ['02']: # simulations with Baum_ghm for ice param    
                ds = xr.open_mfdataset(path+'ds_librad2_05T*_'+source+'_'+solver+'_'+nsim+'.nc',concat_dim='time',combine='nested',
                                      chunks={'height': 50},parallel=True).isel(height=slice(0,140))
                ds.attrs['sim_name'] = 'ds_librad_'+source+'_'+solver+'_'+nsim+''
                ds_list.append(ds)
    return ds_list                
#-------------------------------------
tmp22=load_simulations2(1)
tmp33=load_simulations2(2)
tmp44=load_simulations2(3)
#-------------------------------------
## add to the original datasets 
ds_lib2.append(tmp22[0])
ds_lib2.append(tmp22[1])
ds_lib3.append(tmp33[0])
ds_lib3.append(tmp33[1])
ds_lib4.append(tmp44[0])
ds_lib4.append(tmp44[1])

'''
#-------------------------------------
# libradtrn simulations with NWP clouds 
def load_simulations3(num):
    ds_list = []
    sim = list(simdict.keys())[num]
    print('Working on loading data for', sim)
    path = '/work/bb1135/b381185/icon_output/data_for_crh_unc_paper/offline_radiation_calculation_output/'+sim+'/'
    for solver in ['ipa3d']:
        # loop over sources (thermal/solar)
        for source in ['thermal','solar']:
            # loop over sim types
            for nsim in ['01']: # simulations with ice param of Fu (1998)
                #--------------------------------------------------
                # delta-eddington two-stream using homogenized clouds 
                # with 2.5 km horizontal resolution
                ds1 = xr.open_mfdataset(path+'ds_librad_dl_*_'+source+'_'+solver+'_'+nsim+'.nc',concat_dim='time',combine='nested').isel(height=slice(0,140))
                ds1.attrs['sim_name'] = 'ds_librad_'+source+'_'+solver+'dl_'+nsim+''
                ds_list.append(ds1.rename({'lat': 'lat_2.5', 'lon': 'lon_2.5', 'ddt_radlw': 'ddt_radlw_2.5'}))
                #--------------------------------------------------
                # delta-eddington two-stream using homogenized clouds with fractions 
                # at 2.5 km horizontal resolution
                ds2 = xr.open_mfdataset(path+'ds_librad_cg_*_'+source+'_'+solver+'_'+nsim+'.nc',concat_dim='time',combine='nested').isel(height=slice(0,140))
                ds2.attrs['sim_name'] = 'ds_librad_'+source+'_'+solver+'cg_'+nsim+''
                ds_list.append(ds2.rename({'lat': 'lat_2.5', 'lon': 'lon_2.5', 'ddt_radlw': 'ddt_radlw_2.5'}))
    return ds_list                
#-------------------------------------
ds_lib1=load_simulations3(0)
ds_lib2=load_simulations3(1)
ds_lib3=load_simulations3(2)
ds_lib4=load_simulations3(3)
'''

########################################################
##### Step 2, coarse-graining CRH different calculations
########################################################

def coarse_graining(ds_list,name):
    print(len(ds_list))
    for i in range(len(ds_list)):  

        print('working on dataset: ', i)

        for res in [2.5,5,10,50,100,500]:

            if res == 2.5:
                xbin, ybin = 246, 198
            elif res == 5:
                xbin, ybin = 123, 99
            elif res == 10:
                xbin, ybin = 61, 49
            elif res == 50:
                xbin, ybin = 12, 10
            elif res == 100:
                xbin, ybin = 6, 5
            else:
                xbin, ybin = 1, 1  

            print(f"Resolution: {res}, xbin: {xbin}, ybin: {ybin}")

            tmp = ds_list[i]['ddt_radlw'].groupby_bins("lon", bins=xbin).mean(dim="lon").compute()
            new_variable = tmp.groupby_bins("lat", bins=ybin).mean(dim="lat").compute()

            # Create new dimensions for each resolution
            new_lon_dim = f"lon_{res}"
            new_lat_dim = f"lat_{res}"

            # Extract time and height dimensions from the original dataset
            time_dim = ds_list[i]['time'].values
            height_dim = ds_list[i]['height'].values

            # Align dimensions and assign the new variable to the dataset with new dimensions
            ds_list[i]['ddt_radlw_' + str(res)] = xr.DataArray(new_variable,
                                                               dims=['time', new_lon_dim, new_lat_dim,'height'],
                                                               coords={'time': time_dim,
                                                                       'height': height_dim,
                                                                       new_lon_dim: np.arange(xbin),
                                                                       new_lat_dim: np.arange(ybin)})
        ds_list[i] = ds_list[i].drop_vars(['ddt_dom1','ddt_cs_dom1'])
        ds_list[i].to_netcdf('/work/bb1135/b381185/icon_output/lem_clouds_diag/'+name+'_'+ds_list[i].attrs['sim_name']+'.nc')
        
cg_ds_lib1 = coarse_graining(ds_lib1,'dom_01')
cg_ds_lib2 = coarse_graining(ds_lib2,'dom_02')
cg_ds_lib3 = coarse_graining(ds_lib3,'dom_03')
cg_ds_lib4 = coarse_graining(ds_lib4,'dom_04')

########## for NWP clouds

'''
def coarse_graining(ds_list,name):
    print(len(ds_list))
    for i in range(len(ds_list)):  
        print('working on dataset: ', i)
        for res in [5,10,50,100,500]:

            if res == 5:
                xbin, ybin = 123, 99
            elif res == 10:
                xbin, ybin = 61, 49
            elif res == 50:
                xbin, ybin = 12, 10
            elif res == 100:
                xbin, ybin = 6, 5
            else:
                xbin, ybin = 1, 1  

            print(f"Resolution: {res}, xbin: {xbin}, ybin: {ybin}")

            tmp = ds_list[i]['ddt_radlw_2.5'].groupby_bins("lon_2.5", bins=xbin).mean(dim="lon_2.5").compute()
            new_variable = tmp.groupby_bins("lat_2.5", bins=ybin).mean(dim="lat_2.5").compute()

            # Create new dimensions for each resolution
            new_lon_dim = f"lon_{res}"
            new_lat_dim = f"lat_{res}"

            # Extract time and height dimensions from the original dataset
            time_dim = ds_list[i]['time'].values
            height_dim = ds_list[i]['height'].values

            # Align dimensions and assign the new variable to the dataset with new dimensions
            ds_list[i]['ddt_radlw_' + str(res)] = xr.DataArray(new_variable,
                                                               dims=['time', new_lon_dim, new_lat_dim,'height'],
                                                               coords={'time': time_dim,
                                                                       'height': height_dim,
                                                                       new_lon_dim: np.arange(xbin),
                                                                       new_lat_dim: np.arange(ybin)})
        ds_list[i] = ds_list[i].drop_vars(['ddt_dom1','ddt_cs_dom1'])
        ds_list[i].to_netcdf('/work/bb1135/b381185/icon_output/data_for_crh_unc_paper/offline_radiation_calculation_output/lem_crhun_diag/'+name+'_'+ds_list[i].attrs['sim_name']+'.nc')

cg_ds_lib1 = coarse_graining(ds_lib1,'dom_01')
cg_ds_lib2 = coarse_graining(ds_lib2,'dom_02')
cg_ds_lib3 = coarse_graining(ds_lib3,'dom_03')
cg_ds_lib4 = coarse_graining(ds_lib4,'dom_04')

'''

##################################################
##### Step 3, calculating CRH uncertainty
##################################################

# laoding coarse_grained CRH datasets
def load_datasets1(name):
    ds_list = []
    #print('Working on loading data for', name)
    path = '/work/bb1135/b381185/icon_output/data_for_crh_unc_paper/offline_radiation_calculation_output/lem_crhun_diag/'
    for source in ['thermal','solar']:
        for solver in ['ipa3d','ipa3ddl','ipa3dcg','mysti','mcipa']:
            ds = xr.open_dataset(path+name+'_ds_librad_'+source+'_'+solver+'_01.nc')#,chunks={'time':1 ,'height': 10})
            ds_list.append(ds)
    return ds_list                
#-------------------------------------
ds_dom01=load_datasets1('dom_01')
ds_dom02=load_datasets1('dom_02')
ds_dom03=load_datasets1('dom_03')
ds_dom04=load_datasets1('dom_04')

# laoding coarse_grained CRH datasets ice-optics 02
def load_datasets2(name):
    ds_list = []
    print('Working on loading data for', name)
    path = '/work/bb1135/b381185/icon_output/data_for_crh_unc_paper/offline_radiation_calculation_output/lem_crhun_diag/'
    for source in ['thermal','solar']:
        for solver in ['ipa3d']:
            ds = xr.open_dataset(path+name+'_ds_librad_'+source+'_'+solver+'_02.nc')#,chunks={'time':1 ,'height': 10})
            ds_list.append(ds)
    return ds_list                
#-------------------------------------
tmp02=load_datasets2('dom_02')
tmp03=load_datasets2('dom_03')
tmp04=load_datasets2('dom_04')

ds_dom02.append(tmp02[0])
ds_dom02.append(tmp02[1])
ds_dom03.append(tmp03[0])
ds_dom03.append(tmp03[1])
ds_dom04.append(tmp04[0])
ds_dom04.append(tmp04[1])

# function to calculate local longwave and shortwave CRH uncertainty
def calc_pixel_error(ds1,ds2):
    error_list = []
    # error at 300 m
    variable_name = "ddt_radlw"
    if variable_name in ds1 and variable_name in ds2:
        # error at 300 m
        data1 = ds1["ddt_radlw"]
        data2 = ds2["ddt_radlw"]
        error = (np.abs(data1 - data2).mean(dim=['lat','lon'])).mean('time').compute()
        error_list.append(error)
    # error at other resolutions
    for res in [2.5,5,10,50,100,500]:
        data1 = ds1[f"ddt_radlw_{res}"]
        data2 = ds2[f"ddt_radlw_{res}"]
        error = (np.abs(data1 - data2).mean(dim=[f"lat_{res}",f"lon_{res}"])).mean('time').compute()
        error_list.append(error)
    return error_list

# function to calculate local net CRH uncertainty 
def calc_pixel_error_net(ds1,ds2,ds3,ds4):
    error_list = []
    # error at 300 m
    variable_name = "ddt_radlw"
    if variable_name in ds1 and variable_name in ds3:
        # calc net CRH
        net1 = ds1["ddt_radlw"] + ds2["ddt_radlw"]
        net2 = ds3["ddt_radlw"] + ds4["ddt_radlw"]
        # error at 300 m
        error = (np.abs(net1 - net2).mean(dim=['lat','lon'])).mean('time').compute()
        error_list.append(error)
    # error at other resolutions
    for res in [2.5,5,10,50,100,500]:
        # calc net CRH
        net1 = ds1[f"ddt_radlw_{res}"]+ds2[f"ddt_radlw_{res}"]
        net2 = ds3[f"ddt_radlw_{res}"]+ds4[f"ddt_radlw_{res}"]
        # calc error
        error = (np.abs(net1 - net2).mean(dim=[f"lat_{res}",f"lon_{res}"])).mean('time').compute()
        error_list.append(error)
    return error_list

# function to call above functions for each LEM domain
def calc_errors(ds, indices):
    lw_error = calc_pixel_error(ds[indices[0]], ds[indices[1]])
    sw_error = calc_pixel_error(ds[indices[2]], ds[indices[3]])
    net_error = calc_pixel_error_net(ds[indices[0]], ds[indices[2]], ds[indices[1]], ds[indices[3]])
    return lw_error, sw_error, net_error
'''
Ovrveiw of the indexes
####Thermal:
0=ipa3d, 1=ipa3ddl, 2=ipa3dcg, 3=mysti, 4=mcipa
####Solar:
5=ipa3d, 6=ipa3ddl, 7=ipa3dcg, 8=mysti, 9=mcipa
For domains 2, 3, 4:
10=ipa3d_2 thermal, 11=ipa3d_2 solar
'''
def get_indices_for_error_type(error_type):
    if error_type == '3d':
        return np.array([3, 4, 8, 9])
    elif error_type == 'hg':
        return np.array([0, 1, 5, 6])
    elif error_type == 'vo':
        return np.array([0, 2, 5, 7])
    elif error_type == 'ic':
        return np.array([0, 10, 5, 11])
    else:
        raise ValueError(f'Invalid error type: {error_type}')
        
# Calling the function and doing the calculations
datasets = [ds_dom01]
error_type = ['3d','hg','vo']
# looping over LEM domains and calculating errors
for ds, domain_name in zip(datasets, ['dom01']):
    for etype in error_type:
        indices = get_indices_for_error_type(etype)
        print(f'###### Working on Domain {datasets.index(ds) + 1}, Error Type {etype.upper()}')
        var_name = f'{domain_name}_{etype}_error'
        print(f'{var_name} CRH unc')
        globals()[var_name] = calc_errors(ds, indices)
            
#####################################################################################################            
datasets = [ds_dom02,ds_dom03,ds_dom04]
error_type = ['3d','hg','vo','ic']
# looping over LEM domains and calculating errors
for ds, domain_name in zip(datasets, ['dom02','dom03','dom04']):
    for etype in error_type:
        indices = get_indices_for_error_type(etype)
        print(f'###### Working on Domain {datasets.index(ds) + 1}, Error Type {etype.upper()}')
        var_name = f'{domain_name}_{etype}_error'
        print(f'{var_name} CRH unc')
        globals()[var_name] = calc_errors(ds, indices)      
        
ds = xr.Dataset(
    data_vars={"dom01_3d_lw_error": (("res1", "height"), xr.concat(dom01_3d_error[0], dim="res1").values),
               "dom01_3d_sw_error": (("res1", "height"), xr.concat(dom01_3d_error[1], dim="res1").values),
               "dom01_3d_nt_error": (("res1", "height"), xr.concat(dom01_3d_error[2], dim="res1").values),
               "dom01_hg_lw_error": (("res2", "height"), xr.concat(dom01_hg_error[0], dim="res2").values),
               "dom01_hg_sw_error": (("res2", "height"), xr.concat(dom01_hg_error[1], dim="res2").values),
               "dom01_hg_nt_error": (("res2", "height"), xr.concat(dom01_hg_error[2], dim="res2").values),
               "dom01_vo_lw_error": (("res2", "height"), xr.concat(dom01_vo_error[0], dim="res2").values),
               "dom01_vo_sw_error": (("res2", "height"), xr.concat(dom01_vo_error[1], dim="res2").values),
               "dom01_vo_nt_error": (("res2", "height"), xr.concat(dom01_vo_error[2], dim="res2").values),
               
               "dom02_3d_lw_error": (("res1", "height"), xr.concat(dom02_3d_error[0], dim="res1").values),
               "dom02_3d_sw_error": (("res1", "height"), xr.concat(dom02_3d_error[1], dim="res1").values),
               "dom02_3d_nt_error": (("res1", "height"), xr.concat(dom02_3d_error[2], dim="res1").values),
               "dom02_hg_lw_error": (("res2", "height"), xr.concat(dom02_hg_error[0], dim="res2").values),
               "dom02_hg_sw_error": (("res2", "height"), xr.concat(dom02_hg_error[1], dim="res2").values),
               "dom02_hg_nt_error": (("res2", "height"), xr.concat(dom02_hg_error[2], dim="res2").values),
               "dom02_vo_lw_error": (("res2", "height"), xr.concat(dom02_vo_error[0], dim="res2").values),
               "dom02_vo_sw_error": (("res2", "height"), xr.concat(dom02_vo_error[1], dim="res2").values),
               "dom02_vo_nt_error": (("res2", "height"), xr.concat(dom02_vo_error[2], dim="res2").values),
               "dom02_ic_lw_error": (("res1", "height"), xr.concat(dom02_ic_error[0], dim="res1").values),
               "dom02_ic_sw_error": (("res1", "height"), xr.concat(dom02_ic_error[1], dim="res1").values),
               "dom02_ic_nt_error": (("res1", "height"), xr.concat(dom02_ic_error[2], dim="res1").values),
               
               "dom03_3d_lw_error": (("res1", "height"), xr.concat(dom03_3d_error[0], dim="res1").values),
               "dom03_3d_sw_error": (("res1", "height"), xr.concat(dom03_3d_error[1], dim="res1").values),
               "dom03_3d_nt_error": (("res1", "height"), xr.concat(dom03_3d_error[2], dim="res1").values),
               "dom03_hg_lw_error": (("res2", "height"), xr.concat(dom03_hg_error[0], dim="res2").values),
               "dom03_hg_sw_error": (("res2", "height"), xr.concat(dom03_hg_error[1], dim="res2").values),
               "dom03_hg_nt_error": (("res2", "height"), xr.concat(dom03_hg_error[2], dim="res2").values),
               "dom03_vo_lw_error": (("res2", "height"), xr.concat(dom03_vo_error[0], dim="res2").values),
               "dom03_vo_sw_error": (("res2", "height"), xr.concat(dom03_vo_error[1], dim="res2").values),
               "dom03_vo_nt_error": (("res2", "height"), xr.concat(dom03_vo_error[2], dim="res2").values),
               "dom03_ic_lw_error": (("res1", "height"), xr.concat(dom03_ic_error[0], dim="res1").values),
               "dom03_ic_sw_error": (("res1", "height"), xr.concat(dom03_ic_error[1], dim="res1").values),
               "dom03_ic_nt_error": (("res1", "height"), xr.concat(dom03_ic_error[2], dim="res1").values),
               
               "dom04_3d_lw_error": (("res1", "height"), xr.concat(dom04_3d_error[0], dim="res1").values),
               "dom04_3d_sw_error": (("res1", "height"), xr.concat(dom04_3d_error[1], dim="res1").values),
               "dom04_3d_nt_error": (("res1", "height"), xr.concat(dom04_3d_error[2], dim="res1").values),
               "dom04_hg_lw_error": (("res2", "height"), xr.concat(dom04_hg_error[0], dim="res2").values),
               "dom04_hg_sw_error": (("res2", "height"), xr.concat(dom04_hg_error[1], dim="res2").values),
               "dom04_hg_nt_error": (("res2", "height"), xr.concat(dom04_hg_error[2], dim="res2").values),
               "dom04_vo_lw_error": (("res2", "height"), xr.concat(dom04_vo_error[0], dim="res2").values),
               "dom04_vo_sw_error": (("res2", "height"), xr.concat(dom04_vo_error[1], dim="res2").values),
               "dom04_vo_nt_error": (("res2", "height"), xr.concat(dom04_vo_error[2], dim="res2").values),
               "dom04_ic_lw_error": (("res1", "height"), xr.concat(dom04_ic_error[0], dim="res1").values),
               "dom04_ic_sw_error": (("res1", "height"), xr.concat(dom04_ic_error[1], dim="res1").values),
               "dom04_ic_nt_error": (("res1", "height"), xr.concat(dom04_ic_error[2], dim="res1").values),
               
               
              },
    coords={
        "height": (["height"], np.arange(140)),
        "res1": (["res1"], np.array([0.3, 2.5, 5, 10, 50, 100, 500])),
        "res2": (["res2"], np.array([2.5, 5, 10, 50, 100, 500])),
    }
)
# save the final data
ds.to_netcdf('/work/bb1135/b381185/icon_output/data_for_crh_unc_paper/offline_radiation_calculation_output/lem_crhun_diag/crh_unc_domain_mean.nc')
