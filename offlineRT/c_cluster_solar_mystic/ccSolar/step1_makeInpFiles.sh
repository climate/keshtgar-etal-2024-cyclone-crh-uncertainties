#!/usr/bin/env bash

. VARIABLES

IDENT=$1

pathh=$(cd ../../../ && pwd)
ffname='mysti'

if [ "$IDENT" == "mcipa" ]
then
    MCIPA="mc_ipa"
    ffname='mcipa'
fi

echo
echo "Shell script sucessfully running!"
echo

echo
echo "BUILD FOLDER NAMES AND INPUT FILES"
echo

#First go out of run folder:
cd ..

SCRIPTDIR=$(pwd)

for it in "${time_array[@]}"
do
    echo 'time = ' $it

i=0
for dm in "${dom[@]}"
do
echo 'dom = ' $dm
if [ $it == 05T1000 ]
then
	echo 'sza = ' "${sza_05T1000[$i]}"
	sza="${sza_05T1000[$i]}"
elif [ $it == 05T1030 ]
then
	echo 'sza = ' "${sza_05T1030[$i]}"
        sza="${sza_05T1030[$i]}"
elif [ $it == 05T1100 ]
then
	echo 'sza = ' "${sza_05T1100[$i]}"
        sza="${sza_05T1100[$i]}"
elif [ $it == 05T1130 ]
then
        echo 'sza = ' "${sza_05T1130[$i]}"
        sza="${sza_05T1130[$i]}"
elif [ $it == 05T1200 ]
then
        echo 'sza = ' "${sza_05T1200[$i]}"
        sza="${sza_05T1200[$i]}"
elif [ $it == 05T1230 ]
then
        echo 'sza = ' "${sza_05T1230[$i]}"
        sza="${sza_05T1230[$i]}"
elif [ $it == 05T1300 ]
then
        echo 'sza = ' "${sza_05T1300[$i]}"
        sza="${sza_05T1300[$i]}"
elif [ $it == 05T1330 ]
then
        echo 'sza = ' "${sza_05T1330[$i]}"
        sza="${sza_05T1330[$i]}"	
else
	echo 'sza = ' "${sza_05T1400[$i]}"
        sza="${sza_05T1400[$i]}"	
fi
i=$i+1

    CLOUD_WC_FILE_INP=''$pathh'/wc3d_'$dm'_202201'$it'33Z.dat'
    CLOUD_IC_FILE_INP=''$pathh'/ic3d_'$dm'_202201'$it'33Z.dat'
    ZVALUES="$(head -n2 ${CLOUD_WC_FILE_INP} | tail -n1 | cut -d " " -f 3-)"

      for isim in "${isim_array[@]}"
      do
      echo
      echo "isim = " $isim
      #if [ $isim == 01 ]
      #then
      #   ice_p='fu'
      #   ice_h='#ic_habit rough-aggregate'
      #elif [ $isim == 02 ]
      #then
      #   ice_p='baum_v36'
      #   ice_h='ic_habit ghm'
      #elif [ $isim == 03 ]
      #then
      #   ice_p='baum_v36'
      #   ice_h='ic_habit solid-column'
      #else
      #   ice_p='baum_v36'
      #   ice_h='ic_habit rough-aggregate'
      #fi 	 
      # Create libradtran output file name:
      OUT_FILE=$ffname'_'$it'_'$dm'_isim'$isim'.out'

      echo 'OUT_FILE = ' $OUT_FILE

      FOLDER_NAME=$ffname'_'$it'_'$dm'_isim'$isim

      mkdir $FOLDER_NAME

      cd $FOLDER_NAME

      echo 'Entering folder name = ' $FOLDER_NAME
      # Create output folder for results:
      mkdir output

INP_FILE_NAME='libsetup.inp'
LIBRAD="/home/b/b381185/libRadtran"

############################
# CREATE INPUT FILE HERE !!!
cat > $INP_FILE_NAME << EOF
data_files_path $LIBRAD/data/
atmosphere_file ${pathh}/atmosphere_mean_${dm}_202201${it}33Z.dat

albedo 0.07
source solar
sza ${sza}
#phi ${phi}
mol_abs_param Fu
wavelength_index 1  7
output_process sum

# gas profiles
#mixing_ratio CO2 348
#mixing_ratio O2 209460
#mixing_ratio CH4 1.650 
#mixing_ratio N2O 0.396

mixing_ratio F11 0.0
mixing_ratio F12 0.0
mixing_ratio F22 0.0
mixing_ratio NO2 0.0

mol_modify O4 0.0 DU
mol_modify BRO 0.0 DU
mol_modify OCLO 0.0 DU
mol_modify HCHO 0.0 DU
mol_modify SO2 0.0 DU
mol_modify CO 0.0 DU
mol_modify N2 0.0 DU

#surface
albedo_library IGBP
brdf_rpv_type 17

rte_solver mystic
$MCIPA

mc_forward_output heating K_per_day
mc_photons 72500000

mc_sample_grid 420 343 0.227 0.394

mc_basename $SCRIPTDIR/$FOLDER_NAME/output/$OUT_FILE

wc_properties hu interpolate
ic_properties fu interpolate

wc_file 3D $CLOUD_WC_FILE_INP
ic_file 3D $CLOUD_IC_FILE_INP

EOF
### END OF INPUT FILE ###########
#################################
cat >ther_HR.sh<<EOF
#! /bin/bash

#SBATCH --account=bb1135
#SBATCH --job-name=mystic_dom01.run
#SBATCH --partition=shared
#SBATCH --nodes=1
#SBATCH --output=libther.sh.%j.out
#SBATCH --error=libther.sh.%j.err
#SBATCH --exclusive

cd $(pwd)
echo "Job started @ \$(date)"
$LIBRAD/bin/uvspec < $INP_FILE_NAME
echo "Job finished @ \$(date)"
EOF

        #Leave mysti/mcipa folder:
        cd ..

        done #over isim

done # over dom
done # over itime

echo
echo 
echo 'The END'
