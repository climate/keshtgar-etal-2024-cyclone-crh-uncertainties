#!/usr/bin/env bash

#-------------------------------------------
. VARIABLES

for tt in "${time_array[@]}"
do
for ss in "${isim_array[@]}"
do
   echo "submitting runs for = " $tt'_'$ss

   sbatch s_runs1.sh $tt $ss
   sbatch s_runs2.sh $tt $ss

done 
done
