#!/usr/bin/env bash

. VARIABLES

pathh=$(cd ../../../ && pwd)
ffname='ipa3d'

echo
echo "Shell script sucessfully running!"
echo

echo
echo "BUILD FOLDER NAMES AND INPUT FILES"
echo

#First go out of run folder:
cd ..

SCRIPTDIR=$(pwd)

for it in "${time_array[@]}"
do
    echo 'time = ' $it

for dm in "${dom[@]}"
do
echo 'dom = ' $dm

    CLOUD_WC_FILE_INP=''$pathh'/wc3d_'$dm'_202201'$it'33Z.dat'
    CLOUD_IC_FILE_INP=''$pathh'/ic3d_'$dm'_202201'$it'33Z.dat'
    ZVALUES="$(head -n2 ${CLOUD_WC_FILE_INP} | tail -n1 | cut -d " " -f 3-)"

      for isim in "${isim_array[@]}"
      do
      echo
      echo "isim = " $isim
      if [ $isim == 01 ]
      then
         ice_p='fu'
         ice_h='#ic_habit rough-aggregate'
      elif [ $isim == 02 ]
      then
         ice_p='baum_v36'
         ice_h='ic_habit ghm'
      elif [ $isim == 03 ]
      then
         ice_p='baum_v36'
         ice_h='ic_habit solid-column'
      else
         ice_p='baum_v36'
         ice_h='ic_habit rough-aggregate'
      fi 	 
      # Create libradtran output file name:
      OUT_FILE=$ffname'_'$it'_'$dm'_isim'$isim'.out'

      echo 'OUT_FILE = ' $OUT_FILE

      FOLDER_NAME=$ffname'_'$it'_'$dm'_isim'$isim

      mkdir $FOLDER_NAME

      cd $FOLDER_NAME

      echo 'Entering folder name = ' $FOLDER_NAME
      # Create output folder for results:
      mkdir output

INP_FILE_NAME='libsetup.inp'
LIBRAD="/home/b/b381185/libRadtran"

############################
# CREATE INPUT FILE HERE !!!
cat > $INP_FILE_NAME << EOF
data_files_path $LIBRAD/data/
atmosphere_file ${pathh}/atmosphere_mean_${dm}_202201${it}33Z.dat

albedo 0.009000000000000008
source thermal  
mol_abs_param Fu   
wavelength_index 7  18   
output_process sum   

# gas profiles
#mixing_ratio CO2 348
#mixing_ratio O2 209460
#mixing_ratio CH4 1.650 
#mixing_ratio N2O 0.396

mixing_ratio F11 0.0
mixing_ratio F12 0.0
mixing_ratio F22 0.0
mixing_ratio NO2 0.0

mol_modify O4 0.0 DU
mol_modify BRO 0.0 DU
mol_modify OCLO 0.0 DU
mol_modify HCHO 0.0 DU
mol_modify SO2 0.0 DU
mol_modify CO 0.0 DU
mol_modify N2 0.0 DU

#surface
albedo_library IGBP
brdf_rpv_type 17

rte_solver rodents
ipa_3d

heating_rate layer_fd
mc_backward_output heat K_per_day

mc_sample_grid 420 343 0.227 0.394

mc_basename $SCRIPTDIR/$FOLDER_NAME/output/$OUT_FILE

wc_properties hu interpolate
ic_properties $ice_p interpolate
$ice_h

wc_file 3D $CLOUD_WC_FILE_INP
ic_file 3D $CLOUD_IC_FILE_INP

quiet
EOF
### END OF INPUT FILE ###########
#################################
cat >ther_HR.sh<<EOF
#! /bin/bash

#SBATCH --account=bb1135
#SBATCH --job-name=mystic_dom01.run
#SBATCH --partition=shared
#SBATCH --nodes=1
#SBATCH --output=libther.sh.%j.out
#SBATCH --error=libther.sh.%j.err
#SBATCH --exclusive

cd $(pwd)
echo "Job started @ \$(date)"
$LIBRAD/bin/uvspec < $INP_FILE_NAME
echo "Job finished @ \$(date)"
EOF

        #Leave mysti/mcipa folder:
        cd ..

        done #over isim

done # over dom
done # over itime

echo
echo 
echo 'The END'
