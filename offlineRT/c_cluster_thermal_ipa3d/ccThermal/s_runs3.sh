#!/bin/bash
#SBATCH --partition=compute
#SBATCH --account=bb1135
#SBATCH --nodes=1
#SBATCH --mem=800000
#SBATCH --exclusive
#SBATCH --output=libther.sh.%j.out
#SBATCH --error=libther.sh.%j.err


#---------------------------------
. VARIABLES

pathh=$(cd ../../ && pwd)
ffname='ipa3d'
ssource='thermal'
timee=$1
num=$2

echo "Job started @ $(date)"
for dm in "${dom3[@]}"
do	
/home/b/b381185/libRadtran/bin/uvspec < ${pathh}/c_cluster_${ssource}_${ffname}/${ffname}_${timee}_${dm}_isim${num}/libsetup.inp &
done
wait
echo "Job finished @ $(date)"
