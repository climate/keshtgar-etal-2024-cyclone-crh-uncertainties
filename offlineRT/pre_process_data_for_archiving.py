#@ Behrooz Keshtgar, KIT 2024

# This script is for cleaning up the data for archiving
# and publication

#### Loading libraries
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr

#### Dictionary for loading datasets for the 4 simulations
simdict = {
         'lem_domain01' : {'name':'Shallow cumulus'          ,'res':'300', 'radiation':4, 'mphy':4}, # Only cloud radiation
         'lem_domain02' : {'name':'WCB ascent'               ,'res':'300', 'radiation':4, 'mphy':4}, # Only cloud radiation
         'lem_domain03' : {'name':'WCB cyclonic outflow'     ,'res':'300', 'radiation':4, 'mphy':4}, # Only cloud radiation
         'lem_domain04' : {'name':'WCB anticyclonic outflow' ,'res':'300', 'radiation':4, 'mphy':4}  # Only cloud radiation
          }

##############################################################################################
##### ICON post-processed data
##############################################################################################

# Loading icon datasets
def load_simulations(num):
    d_icon = []
    sim = list(simdict.keys())[num]
    print('Working on loading data for', sim)
    # loading all subdomains
    path_i = '/work/bb1135/b381185/icon_output/data_for_crh_unc_paper/ICON_postprocessed_data/'+sim+'/'
    for time in ['20220105T100033Z','20220105T103033Z','20220105T110033Z','20220105T113033Z','20220105T120033Z','20220105T123033Z','20220105T130033Z','20220105T133033Z','20220105T140033Z']:
        ds_a = []
        for dom in range(1,37):
            ds = xr.open_mfdataset(path_i+'icon2_'+str(dom)+'_'+time+'.nc',concat_dim='lon')[['ddt_radlw','ddt_radsw']].isel(height=slice(0,140))
            ds_a.append(ds) 
        # concat along x dim
        ds_j=[]        
        for j in range(0,36,6):
            ds = xr.concat([ds_a[j],ds_a[j+1],ds_a[j+2],ds_a[j+3],ds_a[j+4],ds_a[j+5]],dim='lon')
            ds_j.append(ds)
        # concat along y dim    
        ds_temp = xr.concat([ds_j[0],ds_j[1],ds_j[2],ds_j[3],ds_j[4],ds_j[5]],dim='lat') 
        # change units to K/day 
        ds_temp['lwcrh'] = ds_temp['ddt_radlw']*86400
        ds_temp['swcrh'] = ds_temp['ddt_radsw']*86400

        ds_temp.coords['lon'] = np.arange(0,344*6)
        ds_temp.coords['lat'] = np.arange(0,281*6)
        ds_temp.coords['time'] = time
        
        # also adding rho
        ds_temp2 = xr.open_dataset(path_i+'icon_rho_'+time+'.nc').isel(height=slice(0,140),lon=slice(1,2064),lat=slice(1,1686))
        ds_mg = xr.merge([ds_temp,ds_temp2])
        d_icon.append(ds_mg)

    # merge them together in time
    ds_icon = xr.concat([d_icon[0],d_icon[1],d_icon[2],d_icon[3],d_icon[4],d_icon[5],d_icon[6]
                     ,d_icon[7],d_icon[8]],dim='time')
    ds = ds_icon.chunk(chunks={'time': 1, 'height': 50})
    return ds
#---------------------------
ds_icon1=load_simulations(0)
ds_icon2=load_simulations(1)
ds_icon3=load_simulations(2)
ds_icon4=load_simulations(3)

# model height at full-levels
z_ifc = xr.open_dataset('/work/bb1135/b381185/icon_output/data_for_crh_unc_paper/ICON_LEM_DOM02_lon40_lat44_300m/icon-atm3d_ML_20220105T120033Z.nc')["z_ifc"].isel(ncells=20000)
z_fl  = ((z_ifc - z_ifc.diff('height_3')/2).values)*1e-3 # km
# select between index 0:140
z_fl2  = z_fl[10:150][::-1]
z_fl2 = xr.DataArray(z_fl2, dims=('height'), coords={'height': ds_icon3['height']})

# assign full model levels to datasets
ds_icon1_u = ds_icon1.assign(z_mc=z_fl2).drop_vars(['ddt_radlw','ddt_radsw'])
ds_icon2_u = ds_icon2.assign(z_mc=z_fl2).drop_vars(['ddt_radlw','ddt_radsw'])
ds_icon3_u = ds_icon3.assign(z_mc=z_fl2).drop_vars(['ddt_radlw','ddt_radsw'])
ds_icon4_u = ds_icon4.assign(z_mc=z_fl2).drop_vars(['ddt_radlw','ddt_radsw'])

# encode and save as nc files
encoding_settings = {var: {'zlib': True, 'complevel': 4} for var in ds_icon3_u.variables}
ds_icon1_u.to_netcdf('/work/bb1135/b381185/icon_output/data_for_crh_unc_paper/postprocessed_data_for_publication/shallow_cumulus/icon_pp_data.nc', encoding=encoding_settings)

encoding_settings = {var: {'zlib': True, 'complevel': 4} for var in ds_icon3_u.variables}
ds_icon2_u.to_netcdf('/work/bb1135/b381185/icon_output/data_for_crh_unc_paper/postprocessed_data_for_publication/WCB_ascent/icon_pp_data.nc', encoding=encoding_settings)

encoding_settings = {var: {'zlib': True, 'complevel': 4} for var in ds_icon3_u.variables}
ds_icon3_u.to_netcdf('/work/bb1135/b381185/icon_output/data_for_crh_unc_paper/postprocessed_data_for_publication/WCB_cyclonic_outflow/icon_pp_data.nc', encoding=encoding_settings)

encoding_settings = {var: {'zlib': True, 'complevel': 4} for var in ds_icon4_u.variables}
ds_icon4_u.to_netcdf('/work/bb1135/b381185/icon_output/data_for_crh_unc_paper/postprocessed_data_for_publication/WCB_anticyclonic_outflow/icon_pp_data.nc', encoding=encoding_settings)

##############################################################################################
##### MYSTIC calculations
##############################################################################################
def load_simulations1(num):
    ds_list = []
    sim = list(simdict.keys())[num]
    print('Working on loading data for', sim)
    path = '/work/bb1135/b381185/icon_output/data_for_crh_unc_paper/offline_radiation_calculation_output/'+sim+'/'
    for solver in ['mysti']:
        # loop over sources (thermal/solar)
        for source in ['thermal','solar']:
            # loop over sim types
            for nsim in ['01']: # simulations with Fu for ice param
                ds = xr.open_mfdataset(path+'ds_librad_05T*_'+source+'_'+solver+'_'+nsim+'.nc',concat_dim='time',combine='nested',
                                       chunks={'height': 50},parallel=True).isel(height=slice(0,140))
                ds.attrs['sim_name'] = 'ds_librad_'+source+'_'+solver+'_'+nsim+''
                ds_list.append(ds)
    return ds_list                
#-------------------------------------
ds_lib1=load_simulations1(0)
ds_lib2=load_simulations1(1)
ds_lib3=load_simulations1(2)
ds_lib4=load_simulations1(3)

names = ['shallow_cumulus','WCB_ascent','WCB_cyclonic_outflow','WCB_anticyclonic_outflow']
for i in range(1,5):
    sim = names[i-1]
    print(sim)
    dataset_name = f'ds_lib{i}'  # Construct the dataset name
    dataset = globals()[dataset_name]
    lwcrh = dataset[0]['ddt_dom1'].isel(time=8)
    swcrh = dataset[1]['ddt_dom1'].isel(time=8)
    # Create a dataset without loading the data into memory
    ds = xr.Dataset(
        data_vars={
            "lwrh": (lwcrh.dims, lwcrh.data),
            "swrh": (swcrh.dims, swcrh.data),
        },
        coords=lwcrh.coords  # Copy coordinates from lwcrh or swcrh
    )
    ds = ds.assign(z_mc=z_fl2)
    ds.attrs['sim_name'] = 'RH from libradtran calculation for '+sim+' domain, LEM clouds, MYSTIC solver, Fu'
    # Save to netCDF without loading the entire dataset into memory
    encoding_settings = {var: {'zlib': True, 'complevel': 4} for var in ds.variables}
    ds.to_netcdf("/work/bb1135/b381185/icon_output/data_for_crh_unc_paper/postprocessed_data_for_publication/"+sim+"/libradtran_pp_mystic_RH_Fu.nc", encoding=encoding_settings)
    
##############################################################################################
##### MYSTIC-ICA calculations
##############################################################################################
def load_simulations1(num):
    ds_list = []
    sim = list(simdict.keys())[num]
    print('Working on loading data for', sim)
    path = '/work/bb1135/b381185/icon_output/data_for_crh_unc_paper/offline_radiation_calculation_output/'+sim+'/'
    for solver in ['mcipa']:
        # loop over sources (thermal/solar)
        for source in ['thermal','solar']:
            # loop over sim types
            for nsim in ['01']: # simulations with Fu for ice param
                ds = xr.open_mfdataset(path+'ds_librad_05T*_'+source+'_'+solver+'_'+nsim+'.nc',concat_dim='time',combine='nested',
                                       chunks={'height': 50},parallel=True).isel(height=slice(0,140))
                ds.attrs['sim_name'] = 'ds_librad_'+source+'_'+solver+'_'+nsim+''
                ds_list.append(ds)
    return ds_list                
#-------------------------------------
ds_lib1=load_simulations1(0)
ds_lib2=load_simulations1(1)
ds_lib3=load_simulations1(2)
ds_lib4=load_simulations1(3)

names = ['shallow_cumulus','WCB_ascent','WCB_cyclonic_outflow','WCB_anticyclonic_outflow']
for i in range(1,5):
    sim = names[i-1]
    print(sim)
    dataset_name = f'ds_lib{i}'  # Construct the dataset name
    dataset = globals()[dataset_name]
    lwcrh = dataset[0]['ddt_radlw']
    swcrh = dataset[1]['ddt_radlw']
    # Create a dataset without loading the data into memory
    ds = xr.Dataset(
        data_vars={
            "lwcrh": (lwcrh.dims, lwcrh.data),
            "swcrh": (swcrh.dims, swcrh.data),
        },
        coords=lwcrh.coords  # Copy coordinates from lwcrh or swcrh
    )
    ds = ds.assign(z_mc=z_fl2)
    ds.attrs['sim_name'] = 'CRH from libradtran calculation for '+sim+' domain, LEM clouds, MYSTIC-ICA solver, Fu'
    # Save to netCDF without loading the entire dataset into memory
    encoding_settings = {var: {'zlib': True, 'complevel': 4} for var in ds.variables}
    ds.to_netcdf("/work/bb1135/b381185/icon_output/data_for_crh_unc_paper/postprocessed_data_for_publication/"+sim+"/libradtran_pp_mystic_ica_Fu.nc", encoding=encoding_settings)
    
##############################################################################################
##### Twostream-Fu calculations
##############################################################################################
def load_simulations1(num):
    ds_list = []
    sim = list(simdict.keys())[num]
    print('Working on loading data for', sim)
    path = '/work/bb1135/b381185/icon_output/data_for_crh_unc_paper/offline_radiation_calculation_output/'+sim+'/'
    for solver in ['ipa3d']:
        # loop over sources (thermal/solar)
        for source in ['thermal','solar']:
            # loop over sim types
            for nsim in ['01']: # simulations with Fu for ice param ['02','03','04'] for Baum-ghm, Baum-sc, Baum-rg
                ds = xr.open_mfdataset(path+'ds_librad2_05T*_'+source+'_'+solver+'_'+nsim+'.nc',concat_dim='time',combine='nested',
                                       chunks={'height': 50},parallel=True).isel(height=slice(0,140))
                ds.attrs['sim_name'] = 'ds_librad_'+source+'_'+solver+'_'+nsim+''
                ds_list.append(ds)
    return ds_list                
#-------------------------------------
ds_lib1=load_simulations1(0)
ds_lib2=load_simulations1(1)
ds_lib3=load_simulations1(2)
ds_lib4=load_simulations1(3)

names = ['shallow_cumulus','WCB_ascent','WCB_cyclonic_outflow','WCB_anticyclonic_outflow']
for i in range(1,5):
    sim = names[i-1]
    print(sim)
    dataset_name = f'ds_lib{i}'  # Construct the dataset name
    dataset = globals()[dataset_name]
    lwcrh = dataset[0]['ddt_radlw']
    swcrh = dataset[1]['ddt_radlw']
    # Create a dataset without loading the data into memory
    ds = xr.Dataset(
        data_vars={
            "lwcrh": (lwcrh.dims, lwcrh.data),
            "swcrh": (swcrh.dims, swcrh.data),
        },
        coords=lwcrh.coords  # Copy coordinates from lwcrh or swcrh
    )
    ds = ds.assign(z_mc=z_fl2)
    ds.attrs['sim_name'] = 'CRH from libradtran calculation for '+sim+' domain, LEM clouds, twostream solver, Fu'
    # Save to netCDF without loading the entire dataset into memory
    encoding_settings = {var: {'zlib': True, 'complevel': 4} for var in ds.variables}
    ds.to_netcdf("/work/bb1135/b381185/icon_output/data_for_crh_unc_paper/postprocessed_data_for_publication/"+sim+"/libradtran_pp_twostr_Fu.nc", encoding=encoding_settings)
    
##############################################################################################
##### twostream- Fu for NWP clouds calculations
##############################################################################################
def load_simulations2(num):
    ds_list = []
    sim = list(simdict.keys())[num]
    print('Working on loading data for', sim)
    path = '/work/bb1135/b381185/icon_output/data_for_crh_unc_paper/offline_radiation_calculation_output/'+sim+'/'
    for solver in ['ipa3d']:
        # loop over sources (thermal/solar)
        for source in ['thermal','solar']:
            # loop over sim types
            for nsim in ['01']: # simulations with ice param of Fu (1998)
                #--------------------------------------------------
                # delta-eddington two-stream using homogenized clouds 
                # with 2.5 km horizontal resolution
                #ds1 = xr.open_mfdataset(path+'ds_librad_dl_*_'+source+'_'+solver+'_'+nsim+'.nc',concat_dim='time',combine='nested').isel(height=slice(0,140))
                #ds1.attrs['sim_name'] = 'ds_librad_'+source+'_'+solver+'dl_'+nsim+''
                #ds_list.append(ds1)
                #--------------------------------------------------
                # delta-eddington two-stream using homogenized clouds with fractions 
                # at 2.5 km horizontal resolution
                ds2 = xr.open_mfdataset(path+'ds_librad_cg_*_'+source+'_'+solver+'_'+nsim+'.nc',concat_dim='time',combine='nested').isel(height=slice(0,140))
                ds2.attrs['sim_name'] = 'ds_librad_'+source+'_'+solver+'cg_'+nsim+''
                ds_list.append(ds2)
    return ds_list

#-------------------------------------
ds_lib1=load_simulations2(0)
ds_lib2=load_simulations2(1)
ds_lib3=load_simulations2(2)
ds_lib4=load_simulations2(3)

names = ['shallow_cumulus','WCB_ascent','WCB_cyclonic_outflow','WCB_anticyclonic_outflow']
for i in range(1,5):
    sim = names[i-1]
    print(sim)
    dataset_name = f'ds_lib{i}'  # Construct the dataset name
    dataset = globals()[dataset_name]
    lwcrh = dataset[0]['ddt_radlw']
    swcrh = dataset[1]['ddt_radlw']
    # Create a dataset without loading the data into memory
    ds = xr.Dataset(
        data_vars={
            "lwcrh": (lwcrh.dims, lwcrh.data),
            "swcrh": (swcrh.dims, swcrh.data),
        },
        coords=lwcrh.coords  # Copy coordinates from lwcrh or swcrh
    )
    ds = ds.assign(z_mc=z_fl2)
    ds.attrs['sim_name'] = 'CRH from libradtran calculation for '+sim+' domain, NWP homogenized clouds with cloud fraction, twostream solver, Fu'
    # Save to netCDF without loading the entire dataset into memory
    encoding_settings = {var: {'zlib': True, 'complevel': 4} for var in ds.variables}
    ds.to_netcdf("/work/bb1135/b381185/icon_output/data_for_crh_unc_paper/postprocessed_data_for_publication/"+sim+"/libradtran_pp_nwpfrcld_twostr_Fu.nc", encoding=encoding_settings)