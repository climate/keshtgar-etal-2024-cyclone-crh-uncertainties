#!/usr/bin/env bash

export time_array=("05T1000" "05T1030" "05T1100" "05T1130" "05T1200" "05T1230" "05T1300" "05T1330" "05T1400")

export dom=("1" "2" "3" "4" "5" "6" "7" "8" "9" "10" "11" "12" "13" "14" "15" "16" "17" "18" "19" "20" "21" "22" "23" "24" "25" "26" "27" "28" "29" "30" "31" "32" "33" "34" "35" "36")

export isim_array=("01")

pathh=$(pwd)
#----------------------------------------------------------------------------------------
ffname1='ipa3d'

for it in "${time_array[@]}"
do
    echo 'time = ' $it

for dm in "${dom[@]}"
do
echo 'dom = ' $dm

for isim in "${isim_array[@]}"
do

if [ -n "$(ls -A $ffname1'_'$it'_'$dm'_isim'$isim/output 2>/dev/null)" ]
then
  echo "file exist (${ffname1}_'${it}'_'${dm}'_isim'${isim}'.out.abs.spc')"
else
  echo "file does not exist (${ffname1}_'${it}'_'${dm}'_isim'${isim}'.out.abs.spc')"
  #count=(($i + 1))
  # let's put these into a file for rerun
  echo "/home/b/b381185/libRadtran/bin/uvspec < ${pathh}/${ffname1}_${it}_${dm}_isim${isim}/libsetup.inp &" >> submit_ipa3d.sh

fi

done
done
done

#-----------------------
