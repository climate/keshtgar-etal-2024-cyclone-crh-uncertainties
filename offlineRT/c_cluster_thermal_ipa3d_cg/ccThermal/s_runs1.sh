#!/bin/bash
#SBATCH --partition=compute
#SBATCH --account=bb1135
#SBATCH --nodes=1
###SBATCH --mem=800000
#SBATCH --exclusive
#SBATCH --output=libther.sh.%j.out
#SBATCH --error=libther.sh.%j.err


#---------------------------------
. VARIABLES

pathh=$(cd ../../ && pwd)
ffname='ipa3d'
ssource='thermal'
#timee="${time_array[0]}"
#timee=$1
num='01'
#num=$2

echo "Job started @ $(date)"
for tt in "${time_array[@]}"
do
for dm in "${dom[@]}"
do	
/home/b/b381185/libRadtran/bin/uvspec < ${pathh}/c_cluster_${ssource}_${ffname}_cg/${ffname}_${tt}_${dm}_isim${num}/libsetup.inp &
done
done
wait
echo "Job finished @ $(date)"
