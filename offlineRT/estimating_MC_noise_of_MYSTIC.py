#@ Behrooz Keshtgar, KIT 2024

# This script is for estimating the Monte Carlo noise of the
# MYSTIC solver

#### Loading libraries
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr

# function to load 10 mystic calculations for the WCB anticyclonic outflow domain at local hour 14:30
def load_datasets(source):
    ds_list = []
    path = '/work/bb1135/b381185/icon_output/data_for_crh_unc_paper/offline_radiation_calculation_output/lem_crhun_diag/ds_for_calc_mc_noise/'
    for nsim in ['01','02','03','04','05','06','07','08','09','10']:
        ds = xr.open_dataset(path+'dom_01_ds_librad_05T1200_'+source+'_mysti_'+nsim+'.nc',
                             chunks={'height': 1}).isel(height=slice(0,140))
        ds.attrs['sim_name'] = 'dom_01_ds_librad_05T1200_solar_mysti_'+nsim+''
        ds_list.append(ds)
    return ds_list

ds_lib_solar = load_datasets('solar')
ds_lib_thermal = load_datasets('thermal')


# Split the ds_lib into two parts, each containing 5 datasets
ds_lib_solar_1 = ds_lib_solar[:5]
ds_lib_solar_2 = ds_lib_solar[5:]

ds_lib_thermal_1 = ds_lib_thermal[:5]
ds_lib_thermal_2 = ds_lib_thermal[5:]

# take the mean over 5 RT calculations
ds_lib_solar_1_mean = xr.concat(ds_lib_solar_1, dim='combine').mean('combine')
ds_lib_solar_2_mean = xr.concat(ds_lib_solar_2, dim='combine').mean('combine')

ds_lib_thermal_1_mean = xr.concat(ds_lib_thermal_1, dim='combine').mean('combine')
ds_lib_thermal_2_mean = xr.concat(ds_lib_thermal_2, dim='combine').mean('combine')

# shortwave
set1_sw = (ds_lib_solar_1_mean['ddt_dom1'].isel(time=0))
set2_sw = (ds_lib_solar_2_mean['ddt_dom1'].isel(time=0))

# longwave
set1_lw = (ds_lib_thermal_1_mean['ddt_dom1'].isel(time=0))
set2_lw = (ds_lib_thermal_2_mean['ddt_dom1'].isel(time=0))

# net
set1_nt = set1_sw + set1_lw
set2_nt = set2_sw + set2_lw

# function to calculate the rsd between two mystic calculation
def calc_rsd(data1,data2):
    # Stack the DataArrays along a new dimension ('combine')
    combined_data = xr.concat([data1, data2], dim='new')

    # Calculate mean, standard deviation
    mean_values = combined_data.mean(dim='new')
    std_values = combined_data.std(dim='new')

    # Calculate the Relative Standard Deviation (RSD) at each pixel
    rsd_values = ((std_values *100) / np.abs(mean_values)).isel(height=slice(0,80))
    
    # load, flatten and get rid of Nan values
    noise = rsd_values.values.flatten()
    noise = noise[~np.isnan(noise)]
    
    # Calculate quartiles, IQR, and bounds to check how many data points falls within the box plot (certain data) i.e.,
    # let's find  what percentage of this noise over all grid boxes are certain. 
    Q1 = np.percentile(noise, 25)
    Q3 = np.percentile(noise, 75)
    IQR = Q3 - Q1
    lower_bound = Q1 - 1.5 * IQR
    upper_bound = Q3 + 1.5 * IQR

    # Count data points within the box
    points_within_box = ((noise >= lower_bound) & (noise <= upper_bound)).sum()
    # Calculate percentage of the data
    perc = (points_within_box/noise.size)*100
    
    return noise, perc

sw_noise, sw_noise_perc = calc_rsd(set1_sw,set2_sw)
lw_noise, lw_noise_perc = calc_rsd(set1_lw,set2_lw)
nt_noise, nt_noise_perc = calc_rsd(set1_nt,set2_nt)

# Plot

# Create sample data (replace with your data)
data = pd.DataFrame({'Shortwave': sw_noise,'Longwave': lw_noise,'Net': nt_noise})

# Visualize the PRSD using a box plot
plt.figure(figsize=(8, 6))
plt.tick_params(labelsize=14)

sns.set(style="whitegrid")
sns.boxplot(data=data,palette='Set3',showfliers=False)
plt.title('Monte carlo noise for shallow cumulus clouds',fontsize=14)
plt.ylabel('Relative standard deviation (%)',fontsize=14)
plt.ylim(0,40)

plt.legend(title='Percentage of certain data points', labels=['Shortwave: '+str(np.round(sw_noise_perc))+'%',
                                                              'Longwave: '+str(np.round(lw_noise_perc))+'%',
                                                              'Net: '+str(np.round(nt_noise_perc))+'%'])
plt.savefig('Fig3_4.png', bbox_inches = 'tight',dpi=300) 
