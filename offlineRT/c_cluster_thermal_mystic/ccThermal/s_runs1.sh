#!/bin/bash
#SBATCH --partition=compute
#SBATCH --account=bb1135
#SBATCH --nodes=1
###SBATCH --mem=256000
#SBATCH --exclusive
#SBATCH --output=libther.sh.%j.out
#SBATCH --error=libther.sh.%j.err


#---------------------------------
. VARIABLES

pathh=$(cd ../../ && pwd)
ffname1='mystic'
ffname2='mysti'
ssource='thermal'
timee=$1
num=$2

echo "Job started @ $(date)"

for dm in "${dom[@]}"
do

/home/b/b381185/libRadtran/bin/uvspec < ${pathh}/c_cluster_${ssource}_${ffname1}/${ffname2}_${timee}_${dm}_isim${num}/libsetup.inp &

done
wait

echo "Job finished @ $(date)"
