# Behrooz Keshtgar, KIT

# loading modules

import xarray as xr
import numpy as np
import pandas as pd
import warnings
warnings.filterwarnings("ignore")


print('start converting mystic/mcipa outputs to netcdf files')
# Dictionary for loading simulations
simdict = {
         'LC1-LES-471x667km-lon25-lat40-300m-0006' : {'res':'300m', 'radiation':4, 'mphy':4}, # Only cloud radiation
         'LC1-LES-471x667km-lon30-lat53-300m-0005' : {'res':'300m', 'radiation':4, 'mphy':4}, # Only cloud radiation
         'LC1-LES-471x667km-lon40-lat44-300m-0003' : {'res':'300m', 'radiation':4, 'mphy':4}, # Only cloud radiation
         'LC1-LES-471x667km-lon50-lat48-300m-0004' : {'res':'300m', 'radiation':4, 'mphy':4}, # Only cloud radiation
          }

# here change the simulation path
simm = list(simdict.keys())[0]
path = '/work/bb1135/icon_output/'+simm+'/input4libradtran/libradtran/'
#---------------------------------------
# number of model layers
height = np.arange(0,150,1)
nz = 150
nx = 420 
ny = 343 
# cutting boundaries
nx2 = 344
ny2 = 281

# solver name for clear-sky hr
solvers_name=['mystic']
k = 0
#---------------------------------------
# loop over time steps
for time in ['05T1000','05T1030','05T1100','05T1130','05T1200','05T1230','05T1300','05T1330','05T1400']:
    # loop over solvers
    for solver in ['mysti','mcipa']:
        # loop over sources (thermal/solar)
        for source in ['thermal','solar']:
            print('working on datasets for:', time+'/'+solver+'/'+source)
            # loop over subdomains
            thr1_3d=[]
            # list holding names of the files for clear-sky results
            sims2=[]
            for dom in range(1,37): 
                # list conatining paths to the results
                simpath = []
                # list holding names of the files
                sims=[]
                
                #----------
                siim = solvers_name[k]+'_'+time+'_'+str(dom)+'_isim01'
                sims2.append(siim)
                #----------
                # loop over number of simulations
                for nsim in ['01','02','03','04','05','06','07','08','09','10']:
                    sim = solver+'_'+time+'_'+str(dom)+'_isim'+nsim
                    simpath.append(path+'c_cluster_'+source+'_mystic/'+sim+'/')
                    sims.append(sim)
          
                # empty list to store heating rates
                thr_3d = []
                for i in range (len(simpath)):
                    print('****loading ascii file:',sims[i])
                    hr = (pd.read_table(simpath[i]+'output/'+sims[i]+'.out.abs.spc',header=None,sep='\s+',usecols=[4]).values)[:,0]
                # change to numpy array    
                tmpp = np.array(thr_3d)
                # take a mean over 10 simulations
                temp = (tmpp[0]+tmpp[1]+tmpp[2]+tmpp[3]+tmpp[4]+tmpp[5]+tmpp[6]+tmpp[7]+tmpp[8]+tmpp[9])/10
                #print('****creating 3d structure')
                temp1 = np.zeros((nx, ny, nz))
                for i in range(nz):
                    temp1[:,:,i] = np.reshape(temp[nx*ny*i:nx*ny*i+nx*ny],(nx,ny))
                # cutting boundaries
                temp2 = temp1[38:420-38,31:343-31,:]    
                thr1_3d.append(temp2*1.4) #  *cp/cv factor in ICON
                del tmpp

            # loading clear sky heating rates
            cshr = []
            print('****loading clear-sky hr')
            for i in range (len(sims2)):
                cshr_t = (pd.read_table(path+source+'_clear_sky/output/'+sims2[i].replace('isim'+nsim,'cs_hr')+'.out',header=None,sep='\s+',usecols=[1]).values)[:,0]
                tmp = cshr_t*1.4 # x cp/cv factor in ICON 
                # broadcasting to 3D structure
                tmp2 = np.broadcast_to(tmp,(nx2,ny2,nz)).copy()
                cshr.append(tmp2)
                
            print('****create dataset')
            # creating datasets

            ds = xr.Dataset({
            #'half_levels': xr.DataArray(z1d, dims=('height')),
            'ddt_dom1': xr.DataArray(thr1_3d[0], dims=('lon','lat','height')),
            'ddt_dom2': xr.DataArray(thr1_3d[1], dims=('lon','lat','height')),
            'ddt_dom3': xr.DataArray(thr1_3d[2], dims=('lon','lat','height')),
            'ddt_dom4': xr.DataArray(thr1_3d[3], dims=('lon','lat','height')),
            'ddt_dom5': xr.DataArray(thr1_3d[4], dims=('lon','lat','height')),
            'ddt_dom6': xr.DataArray(thr1_3d[5], dims=('lon','lat','height')),
            'ddt_dom7': xr.DataArray(thr1_3d[6], dims=('lon','lat','height')),
            'ddt_dom8': xr.DataArray(thr1_3d[7], dims=('lon','lat','height')),
            'ddt_dom9': xr.DataArray(thr1_3d[8], dims=('lon','lat','height')),
            'ddt_dom10': xr.DataArray(thr1_3d[9], dims=('lon','lat','height')),
            'ddt_dom11': xr.DataArray(thr1_3d[10], dims=('lon','lat','height')),
            'ddt_dom12': xr.DataArray(thr1_3d[11], dims=('lon','lat','height')),
            'ddt_dom13': xr.DataArray(thr1_3d[12], dims=('lon','lat','height')),
            'ddt_dom14': xr.DataArray(thr1_3d[13], dims=('lon','lat','height')),
            'ddt_dom15': xr.DataArray(thr1_3d[14], dims=('lon','lat','height')),
            'ddt_dom16': xr.DataArray(thr1_3d[15], dims=('lon','lat','height')),
            'ddt_dom17': xr.DataArray(thr1_3d[16], dims=('lon','lat','height')),
            'ddt_dom18': xr.DataArray(thr1_3d[17], dims=('lon','lat','height')),
            'ddt_dom19': xr.DataArray(thr1_3d[18], dims=('lon','lat','height')),
            'ddt_dom20': xr.DataArray(thr1_3d[19], dims=('lon','lat','height')),
            'ddt_dom21': xr.DataArray(thr1_3d[20], dims=('lon','lat','height')),
            'ddt_dom22': xr.DataArray(thr1_3d[21], dims=('lon','lat','height')),
            'ddt_dom23': xr.DataArray(thr1_3d[22], dims=('lon','lat','height')),
            'ddt_dom24': xr.DataArray(thr1_3d[23], dims=('lon','lat','height')),
            'ddt_dom25': xr.DataArray(thr1_3d[24], dims=('lon','lat','height')),
            'ddt_dom26': xr.DataArray(thr1_3d[25], dims=('lon','lat','height')),
            'ddt_dom27': xr.DataArray(thr1_3d[26], dims=('lon','lat','height')),
            'ddt_dom28': xr.DataArray(thr1_3d[27], dims=('lon','lat','height')),
            'ddt_dom29': xr.DataArray(thr1_3d[28], dims=('lon','lat','height')),
            'ddt_dom30': xr.DataArray(thr1_3d[29], dims=('lon','lat','height')),
            'ddt_dom31': xr.DataArray(thr1_3d[30], dims=('lon','lat','height')),
            'ddt_dom32': xr.DataArray(thr1_3d[31], dims=('lon','lat','height')),
            'ddt_dom33': xr.DataArray(thr1_3d[32], dims=('lon','lat','height')),
            'ddt_dom34': xr.DataArray(thr1_3d[33], dims=('lon','lat','height')),
            'ddt_dom35': xr.DataArray(thr1_3d[34], dims=('lon','lat','height')),
            'ddt_dom36': xr.DataArray(thr1_3d[35], dims=('lon','lat','height')),

            'ddt_cs_dom1': xr.DataArray(cshr[0], dims=('lon','lat','height')),
            'ddt_cs_dom2': xr.DataArray(cshr[1], dims=('lon','lat','height')),
            'ddt_cs_dom3': xr.DataArray(cshr[2], dims=('lon','lat','height')),
            'ddt_cs_dom4': xr.DataArray(cshr[3], dims=('lon','lat','height')),
            'ddt_cs_dom5': xr.DataArray(cshr[4], dims=('lon','lat','height')),
            'ddt_cs_dom6': xr.DataArray(cshr[5], dims=('lon','lat','height')),
            'ddt_cs_dom7': xr.DataArray(cshr[6], dims=('lon','lat','height')),
            'ddt_cs_dom8': xr.DataArray(cshr[7], dims=('lon','lat','height')),
            'ddt_cs_dom9': xr.DataArray(cshr[8], dims=('lon','lat','height')),
            'ddt_cs_dom10': xr.DataArray(cshr[9], dims=('lon','lat','height')),
            'ddt_cs_dom11': xr.DataArray(cshr[10], dims=('lon','lat','height')),
            'ddt_cs_dom12': xr.DataArray(cshr[11], dims=('lon','lat','height')),
            'ddt_cs_dom13': xr.DataArray(cshr[12], dims=('lon','lat','height')),
            'ddt_cs_dom14': xr.DataArray(cshr[13], dims=('lon','lat','height')),
            'ddt_cs_dom15': xr.DataArray(cshr[14], dims=('lon','lat','height')),
            'ddt_cs_dom16': xr.DataArray(cshr[15], dims=('lon','lat','height')),
            'ddt_cs_dom17': xr.DataArray(cshr[16], dims=('lon','lat','height')),
            'ddt_cs_dom18': xr.DataArray(cshr[17], dims=('lon','lat','height')),
            'ddt_cs_dom19': xr.DataArray(cshr[18], dims=('lon','lat','height')),
            'ddt_cs_dom20': xr.DataArray(cshr[19], dims=('lon','lat','height')),
            'ddt_cs_dom21': xr.DataArray(cshr[20], dims=('lon','lat','height')),
            'ddt_cs_dom22': xr.DataArray(cshr[21], dims=('lon','lat','height')),
            'ddt_cs_dom23': xr.DataArray(cshr[22], dims=('lon','lat','height')),
            'ddt_cs_dom24': xr.DataArray(cshr[23], dims=('lon','lat','height')),
            'ddt_cs_dom25': xr.DataArray(cshr[24], dims=('lon','lat','height')),
            'ddt_cs_dom26': xr.DataArray(cshr[25], dims=('lon','lat','height')),
            'ddt_cs_dom27': xr.DataArray(cshr[26], dims=('lon','lat','height')),
            'ddt_cs_dom28': xr.DataArray(cshr[27], dims=('lon','lat','height')),
            'ddt_cs_dom29': xr.DataArray(cshr[28], dims=('lon','lat','height')),
            'ddt_cs_dom30': xr.DataArray(cshr[29], dims=('lon','lat','height')),
            'ddt_cs_dom31': xr.DataArray(cshr[30], dims=('lon','lat','height')),
            'ddt_cs_dom32': xr.DataArray(cshr[31], dims=('lon','lat','height')),
            'ddt_cs_dom33': xr.DataArray(cshr[32], dims=('lon','lat','height')),
            'ddt_cs_dom34': xr.DataArray(cshr[33], dims=('lon','lat','height')),
            'ddt_cs_dom35': xr.DataArray(cshr[34], dims=('lon','lat','height')),
            'ddt_cs_dom36': xr.DataArray(cshr[35], dims=('lon','lat','height')),

            },
            coords={"lat": (["lat"], np.arange(0,ny2)), 
                            "lon": (["lon"], np.arange(0,nx2)),
                            'height':(["height"],np.arange(0,nz)),})

            # let's merge them together here
            print('****merging subdomains into the big domain')
            ds_j = []
            ds_j2 = []
            # concat along x dim
            for j in range(1,37,6):
                ds_i = xr.concat([ds['ddt_dom'+str(j)+''],ds['ddt_dom'+str(j+1)+''],
                                  ds['ddt_dom'+str(j+2)+''],ds['ddt_dom'+str(j+3)+''],
                                  ds['ddt_dom'+str(j+4)+''],ds['ddt_dom'+str(j+5)+'']],dim='lon')
                ds_j.append(ds_i)

                ds_i2 = xr.concat([ds['ddt_cs_dom'+str(j)+''],ds['ddt_cs_dom'+str(j+1)+''],
                                  ds['ddt_cs_dom'+str(j+2)+''],ds['ddt_cs_dom'+str(j+3)+''],
                                  ds['ddt_cs_dom'+str(j+4)+''],ds['ddt_cs_dom'+str(j+5)+'']],dim='lon')
                ds_j2.append(ds_i2)
            # concat along y dim    
            ds_f1 = xr.concat([ds_j[0],ds_j[1],ds_j[2],ds_j[3],ds_j[4],ds_j[5]],dim='lat')
            ds_f2 = xr.concat([ds_j2[0],ds_j2[1],ds_j2[2],ds_j2[3],ds_j2[4],ds_j2[5]],dim='lat')
            ds_1 = xr.merge([ds_f1,ds_f2])
            ds_1['ddt_radlw'] = ds_1['ddt_dom1'] - ds_1['ddt_cs_dom1']
            ds_1.coords['lon'] = np.arange(0,344*6)
            ds_1.coords['lat'] = np.arange(0,281*6)
            ds_1 = ds_1.expand_dims('time')
            print('****save')
            ds_1.to_netcdf('/work/bb1135/icon_output/'+simm+'/input4libradtran/libradtran/output_netcdf/ds_librad_'+time+'_'+source+'_'+solver+'_01.nc')
            print('----------------------------------------------------------------------------')

print('finished')            
