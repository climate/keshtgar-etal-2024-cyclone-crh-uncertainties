# Code repository for the publication "Uncertainties in cloud-radiative heating within an idealized extratropical cyclone".

**Author:** Behrooz Keshtgar, IMKTRO, Karlsruhe Institute of Technology, behrooz.keshtgar@kit.edu

**The repository contains:**

* **sims:** ICON model setup
  - Baroclinic Life Cycle Simulation (ICON-NWP)
  - Large Eddy Model Simulations (ICON-LEM)

* **offlineRT:** Procedures for offline radiative transfer calculations using LibRadTran
  - Scripts for post-processing LEM simulation output for use in offline radiative transfer calculations
  - Python scripts for post-processing LibRadTran results
  - Bash scripts for running LibRadTran
  - Python scripts for preprocessing and data archiving

* **plots4paper:** jupyter notebooks for figures used in papers, also figure pdfs

The simulation’s raw outputs are archived on the High-Performance Storage System at the German Climate Computing Center (DKRZ). The post-processed data for producing the figures in the manuscript along with a copy of the Git repository are published in an online repository at https://doi.org/10.5281/zenodo.10807815 
