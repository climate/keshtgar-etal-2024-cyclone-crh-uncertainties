#! /bin/bash
set -x
ulimit -s unlimited
#=============================================================================
# OpenMP environment variables
# ----------------------------
export OMP_NUM_THREADS=4
export ICON_THREADS=$OMP_NUM_THREADS
export OMP_SCHEDULE="guided"
export OMP_DYNAMIC="false"
export OMP_STACKSIZE=500M
#
# MPI variables
# -------------
no_of_nodes=${SLURM_JOB_NUM_NODES:=1}
mpi_procs_pernode=$((128 / OMP_NUM_THREADS))
((mpi_total_procs=no_of_nodes * mpi_procs_pernode))

ulimit -s $((4 * 1024 * 1024))
ulimit -c 0


# Loading modules 

module purge
module load cdo/2.0.5-gcc-11.2.0
module load python3/2022.01-gcc-11.2.0
module load nco/5.0.6-gcc-11.2.0

module list

#--------------------------------------------------------------------------------------
# ICONTOOLS directory

ICONTOOLS_DIR=/home/b/b381185/dwd_icon_tools/icontools

BINARY_ICONSUB=iconsub
BINARY_REMAP=iconremap
BINARY_GRIDGEN=icongridgen

#--------------------------------------------------------------------------------------
# here choose the grid for different LEM domains
# dom01: shallow cumulus clouds   lon_center=25.0, lat_center=40.0
# dom02: WCB ascent region        lon_center=40.0, lat_center=44.0
# dom03: WCB cyclonic outflow     lon_center=30.0, lat_center=53.0
# dom04: WCB anticyclonic otflow  lon_center=50.0, lat_center=48.0


mkdir plane_nest_300m_r6x6_2mom_25_40_0001
cd plane_nest_300m_r6x6_2mom_25_40_0001

# import the grid from grid_generator
gridfile=raggedOrthogonal_471x667_300_with_boundary_dom01.nc
cp /work/bb1135/from_Mistral/bb1135/b381185/tools/GridGenerator_master/grids/$gridfile ./
#---------------------------------------------------------------------------------------
# 1- Remmaping the external prameters file onto the nest grid
# Aquaplanet extpar file

for field in SOILTYP FR_LAND ICE PLCOV_MX LAI_MX RSMIN URBAN FOR_D FOR_E EMIS_RAD ROOTDP Z0 NDVI_MAX topography_c SSO_STDH SSO_THETA SSO_GAMMA SSO_SIGMA T_CL FR_LAKE DEPTH_LK topography_v LU_CLASS_FRACTION NDVI NDVI_MRAT AER_BC AER_DUST AER_ORG AER_SO4 AER_SS ALB ALNID ALUVD lon lat clon clat clon_vertices clat_vertices ; do

cat >> NAMELIST_ICONREMAP_FIELDS << EOF_2A
!
&input_field_nml
 inputname      = "${field}"
 outputname     = "${field}"
 intp_method    = 3
/
EOF_2A

done

cat NAMELIST_ICONREMAP_FIELDS

cat > NAMELIST_ICONREMAP << EOF_2C
&remap_nml
 in_grid_filename  = '../inputs/icon_grid_0010_R02B04_G.nc'
 in_filename       = '../inputs/icon_extpar_0010_R02B04_G_aquaplanet.nc'
 in_type           = 2
 out_grid_filename = '$gridfile'
 out_filename      = 'extpar_remapped.nc'
 out_type          = 2
 out_filetype      = 4
 l_have3dbuffer    = .false.
 ncstorage_file    = "ncstorage.tmp"
/
EOF_2C


${ICONTOOLS_DIR}/${BINARY_REMAP} \
            --remap_nml NAMELIST_ICONREMAP                                  \
            --input_field_nml NAMELIST_ICONREMAP_FIELDS 2>&1

#-----------------------------------------------------------------------------
# clean-up

rm -f ncstorage.tmp*
rm -f nml.log  NAMELIST_SUB NAMELIST_ICONREMAP NAMELIST_ICONREMAP_FIELDS

#-----------------------------------------------------------------------------


# 4- Remmaping the Ozone file onto the channel grid

# APE O3 file for irad_o3 = 4

for field in O3 ; do

cat >> NAMELIST_ICONREMAP_FIELDS << EOF_2A
!
&input_field_nml
 inputname      = "${field}"
 outputname     = "${field}"
 intp_method    = 3
/
EOF_2A

done

cat > NAMELIST_ICONREMAP << EOF_2C
&remap_nml
 in_grid_filename  = ''
 in_filename       = '../inputs/ape_o3_R2B04_1Pa_cell.t63grid.nc'
 in_type           = 1
 out_grid_filename = '$gridfile'
 out_filename      = 'ape_O3_remapped.nc'
 out_type          = 2
 out_filetype      = 4
 l_have3dbuffer    = .false.
 ncstorage_file    = "ncstorage.tmp"
/
EOF_2C

${ICONTOOLS_DIR}/${BINARY_REMAP} \
            --remap_nml NAMELIST_ICONREMAP                                  \
            --input_field_nml NAMELIST_ICONREMAP_FIELDS 2>&1

#-----------------------------------------------------------------------------
# clean-up

rm -f ncstorage.tmp*
rm -f nml.log  NAMELIST_SUB NAMELIST_ICONREMAP NAMELIST_ICONREMAP_FIELDS

#-----------------------------------------------------------------------------
# Correction of remmaped extpar file 

ncatted -a rawdata,global,c,c,"GLOBCOVER2009, FAO DSMW, GLOBE, Lake Database" extpar_remapped.nc

# Run the python script for correction of the type and month dimension

python ../inputs/extpar_helper.py

# modification of grid global attributes
python ../inputs/grid_change.py $gridfile
#----------------------------------------------------------------------------

# Correction of remmaped ozone file

ncrename -d plev,level ape_O3_remapped.nc
ncrename -v plev,level ape_O3_remapped.nc

# clean 
rm extpar_remapped.nc 




