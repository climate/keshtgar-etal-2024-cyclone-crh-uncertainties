#! /bin/bash
set -x
ulimit -s unlimited
#=============================================================================
# OpenMP environment variables
# ----------------------------
export OMP_NUM_THREADS=4
export ICON_THREADS=$OMP_NUM_THREADS
export OMP_SCHEDULE="guided"
export OMP_DYNAMIC="false"
export OMP_STACKSIZE=500M
#
# MPI variables
# -------------
no_of_nodes=${SLURM_JOB_NUM_NODES:=1}
mpi_procs_pernode=$((128 / OMP_NUM_THREADS))
((mpi_total_procs=no_of_nodes * mpi_procs_pernode))

ulimit -s $((4 * 1024 * 1024))
ulimit -c 0


# Loading modules

module purge
module load cdo/2.0.5-gcc-11.2.0
module load python3/2022.01-gcc-11.2.0
module load nco/5.0.6-gcc-11.2.0

module list

#--------------------------------------------------------------------------------------
# ICONTOOLS directory

ICONTOOLS_DIR=/home/b/b381185/dwd_icon_tools/icontools

BINARY_ICONSUB=iconsub
BINARY_REMAP=iconremap
BINARY_GRIDGEN=icongridgen

#=============================================================================
# here choose the grid for different LEM domains
# dom01: shallow cumulus clouds   lon_center=25.0, lat_center=40.0
# dom02: WCB ascent region        lon_center=40.0, lat_center=44.0
# dom03: WCB cyclonic outflow     lon_center=30.0, lat_center=53.0
# dom04: WCB anticyclonic otflow  lon_center=50.0, lat_center=48.0
#=============================================================================

EXPNAME=plane_nest_300m_r6x6_2mom_25_40_0001

gridfile=raggedOrthogonal_471x667_300_with_boundary_dom01.nc

INDATDIR=/work/bb1135/icon_output/LC1-channel-4000x9000km-2km-0002  # the folder for the icon-nwp input
OUTDATDIR=/work/bb1135/LES_Simulations/initial_conditions/${EXPNAME}    # the folder for the remapped output

in_grid_File=/work/bb1135/icon_output/LC1-channel-4000x9000km-2km-0002/grid_DOM01.nc
out_grid_File=/work/bb1135/LES_Simulations/initial_conditions/${EXPNAME}/$gridfile

in_data_File=${INDATDIR}/icon-fg_ML_202201
out_data_File=${OUTDATDIR}/icon-fg_ML_nest

### interpolation method
# 2 -> interpolation : conservative (gives problems in the outer most cells in HDCP2-DE)
# 3 -> interpolation : rbf: scalar  (RBF method, needs correct values for rbf_scale_scalar)
intp_method=3

# the directory for the experiment will be created, if not already there
if [ ${OUTDATDIR}.notset = .notset ]; then
    echo "OUTDATDIR not set"
    exit 1
fi

if [ ! -d $OUTDATDIR ]; then
    mkdir -p $OUTDATDIR
fi
#
cd $OUTDATDIR
#

for day in 05 ; do

for hour in 06 ; do 

rm -f indata.nc indata-vn.nc
cp ${in_data_File}${day}T${hour}0030Z.nc indata.nc
# add grid to in_data_File, needed for vn interpolation
# ATTENTION: make sure to use cdo/1.7.0-magicsxx-gcc48, other cdo version can lead
# to a crash of the vn remapping with the dwdicontools

cdo -P 32 setgrid,${in_grid_File} -selname,vn ${in_data_File}${day}T${hour}0030Z.nc indata-vn.nc

# create ICON master namelist: obtained from Matthias Brueck
# ------------------------
cat > ${OUTDATDIR}/tmp.nml << REMAP_NML_EOF
! REMAPPING NAMELIST FILE
!
&remap_nml
 in_grid_filename   = '${in_grid_File}'
 in_filename        = 'indata.nc'
 in_type            = 2
 out_grid_filename  = '${out_grid_File}'
 out_filename       = 'outdata.nc'
 out_type           = 2
 out_filetype       = 5
 !s_maxsize         = 1000000
 lsynthetic_grid    = .FALSE.
/
REMAP_NML_EOF

for field in  w rho theta_v qv qc qi qr qs tke u v pres_sfc temp pres z_ifc t_2m td_2m u_10m v_10m fr_land gz0 t_g t_ice h_ice alb_si qv_s fr_seaice t_sk t_seasfc w_i t_so w_so w_so_ice t_snow w_snow rho_snow h_snow freshsnow snowfrac_lc rho_snow_mult t_snow_mult wliq_snow wtot_snow dzh_snow ; do 

cat >> ${OUTDATDIR}/tmp.nml << REMAP_NML_EOF 
! 
&input_field_nml  
 inputname      = "${field}"  
 outputname     = "${field}"  
 intp_method    = ${intp_method} 
/ 
REMAP_NML_EOF

done

cat > ${OUTDATDIR}/tmp-vn.nml << REMAP_NML_EOF
! REMAPPING NAMELIST FILE
!
&remap_nml
 in_grid_filename   = '${in_grid_File}'
 in_filename        = 'indata-vn.nc'
 in_type            = 2
 out_grid_filename  = '${out_grid_File}'
 out_filename       = 'outdata-vn.nc'
 out_type           = 2
 out_filetype       = 5
 !s_maxsize         = 1000000
 lsynthetic_grid    = .FALSE.
/
! DEFINITION FOR INPUT DATA FIELD
!
&input_field_nml
 inputname      = "vn"
 outputname     = "vn"
!! intp_method    = ${intp_method}
/
REMAP_NML_EOF

# Hint: option -vvvvvvvvvvvv activates a lot of diagnostic output
#${START} ${ICONTOOLS_DIR}/${BINARY_REMAP} --remap_nml ${OUTDATDIR}/tmp.nml    2>&1
#${START} ${ICONTOOLS_DIR}/${BINARY_REMAP} --remap_nml ${OUTDATDIR}/tmp-vn.nml 2>&1

${ICONTOOLS_DIR}/${BINARY_REMAP} --remap_nml ${OUTDATDIR}/tmp.nml    2>&1
${ICONTOOLS_DIR}/${BINARY_REMAP} --remap_nml ${OUTDATDIR}/tmp-vn.nml 2>&1

# merge interpolated output
cdo -O merge outdata.nc outdata-vn.nc ${out_data_File}_202201${day}T${hour}0030Z.nc

# clean up
rm -f indata.nc indata-vn.nc outdata.nc outdata-vn.nc

done # end of loop over times (hours)

done
##cd ${RUNSCRIPTDIR}
