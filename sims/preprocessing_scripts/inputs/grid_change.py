import xarray as xr
import sys


grid = xr.open_dataset(sys.argv[1])
grid.attrs['grid_geometry'] = 3.
grid.attrs['domain_length'] = grid.attrs['domain_length']*3

grid.to_netcdf('./new_'+str(sys.argv[1]))
