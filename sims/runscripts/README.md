** ICON simulations**

*Baroclinic life cycle simulation with ICON-NWP**

LC1-channel-4000x9000km-2km-0002        : Only with cloud radiaton, two-moment microphysics, and ecrad

*Large eddy model simulations with ICON-LEM*

WCB ascent:

LC1-LES-471x667km-lon40-lat44-300m-0003 : Only with cloud radiaton, two-moment microphysics, ecrad, LATBC: NWP_0002

WCB anticyclonic outflow:

LC1-LES-471x667km-lon50-lat48-300m-0004 : Only with cloud radiaton, two-moment microphysics, ecrad, LATBC: NWP_0002

WCB cyclonic outflow:

LC1-LES-471x667km-lon30-lat53-300m-0005 : Only with cloud radiaton, two-moment microphysics, ecrad, LATBC: NWP_0002

Shallow cumulus:

LC1-LES-471x667km-lon25-lat40-300m-0006 : Only with cloud radiaton, two-moment microphysics, ecrad, LATBC: NWP_0002

